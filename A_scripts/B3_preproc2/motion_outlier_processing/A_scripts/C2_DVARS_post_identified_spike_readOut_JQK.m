% get multiple outlier indices for each subject and run according to
% timecourses of good ICs (not removed during denoising)
clear all

pn.root = '/Volumes/FB-LIP/Projects/StateSwitch/dynamic/data/mri/task/preproc/A_scripts/B3_preproc2/motion_outlier_processing/';

ID = {'1117' '1118' '1120' '1124' '1125' '1126' '1131' '1132' '1135' '1136' '1151' '1160' '1164' '1167' '1169' '1172' '1173' '1178' '1182' '1214' '1215' '1216' '1219' '1223' '1227' '1228' '1233' '1234' '1237' '1239' '1240' '1243' '1245' '1247' '1250' '1252' '1257' '1261' '1265' '1266' '1268' '1270' '1276' '1281' '2104' '2107' '2108' '2112' '2118' '2120' '2121' '2123' '2125' '2129' '2130' '2131' '2132' '2133' '2134' '2135' '2139' '2140' '2142' '2145' '2147' '2149' '2157' '2160' '2201' '2202' '2203' '2205' '2206' '2209' '2210' '2211' '2213' '2214' '2215' '2216' '2217' '2219' '2222' '2224' '2226' '2227' '2236' '2237' '2238' '2241' '2244' '2246' '2248' '2250' '2251' '2252' '2253' '2254' '2255' '2258' '2261'};
run_ID = {'run-1' 'run-2' 'run-3' 'run-4'};

figure
for thresh = 3:6
    %thresh = 5; % SD outlier threshold
    load([pn.root, 'comp2_IC_thr-' num2str(thresh) '_DVARS_post.mat'],'out');  

    NumCompsBoth = [];
    NumCompsDVARS = [];
    NumCompsIC = [];
    for sub = 1:length(ID)
        for run = 1:length(run_ID)
            NumCompsBoth(sub) = out.(['sub' ID{sub}]).total_overlap_dvars_IC_N;
            NumCompsDVARS(sub) = out.(['sub' ID{sub}]).total_dvars_not_IC_N;
            NumCompsIC(sub) = out.(['sub' ID{sub}]).total_IC_not_dvars;
        end
    end

    %figure; imagesc([NumCompsBoth; NumCompsDVARS; NumCompsIC]');

    subplot(2,2,thresh-2)
    bar(nanmean([NumCompsBoth; NumCompsDVARS; NumCompsIC]',1));
    ylabel('Number of detected spike volumes')
    %xlabel('Type');
    xticklabels({'Overlap', 'DVARS exclusive', 'IC exclusive'})
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    title(['Threshold ', num2str(thresh)]);
end