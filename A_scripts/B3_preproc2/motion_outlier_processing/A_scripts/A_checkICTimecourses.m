% get multiple outlier indices for each subject and run according to
% timecourses of good ICs (not removed during denoising)
clear all
cd('/Volumes/FB-LIP/Projects/StateSwitch/dynamic/data/mri/task/preproc/A_scripts/B3_preproc2/motion_outlier_processing')

ID = {'1117' '1118' '1120' '1124' '1125' '1126' '1131' '1132' '1135' '1136' '1151' '1160' '1164' '1167' '1169' '1172' '1173' '1178' '1182' '1214' '1215' '1216' '1219' '1223' '1227' '1228' '1233' '1234' '1237' '1239' '1240' '1243' '1245' '1247' '1250' '1252' '1257' '1261' '1265' '1266' '1268' '1270' '1276' '1281' '2104' '2107' '2108' '2112' '2118' '2120' '2121' '2123' '2125' '2129' '2130' '2131' '2132' '2133' '2134' '2135' '2139' '2140' '2142' '2145' '2147' '2149' '2157' '2160' '2201' '2202' '2203' '2205' '2206' '2209' '2210' '2211' '2213' '2214' '2215' '2216' '2217' '2219' '2222' '2224' '2226' '2227' '2236' '2237' '2238' '2241' '2244' '2246' '2248' '2250' '2251' '2252' '2253' '2254' '2255' '2258' '2261'};
run_ID = {'run-1' 'run-2' 'run-3' 'run-4'};

thresh = 3; % SD outlier threshold

for sub = 1:length(ID)
    for run = 1:length(run_ID)
        
     % get good components for run
     HLN=['/Volumes/FB-LIP/Projects/StateSwitch/dynamic/data/mri/task/preproc/B_data/D_preproc/' ID{sub} '/preproc2/' run_ID{run} '/FEAT.feat/hand_labels_noise.txt'];
     
     % check if run exists
     if ~isfile(HLN)
        fprintf('No %s for subject %s ... skipping\n',run_ID{run},ID{sub})
        continue
     else
         fprintf('Processing subject %s %s\n',ID{sub},run_ID{run})
     end
     
     [~,IC_noise] = system(['tail -n -1 ' HLN]);
     IC_noise = str2num(IC_noise);
     IC_good = setdiff(1:30,IC_noise);
     
     % get total number of good components
     good_IC_N = length(IC_good);
     
     % get length of time-series
     scans=load(['/Volumes/FB-LIP/Projects/StateSwitch/dynamic/data/mri/task/preproc/B_data/D_preproc/' ID{sub} '/preproc2/' run_ID{run} '/FEAT.feat/filtered_func_data.ica/report/t1.txt']);
     scans=length(scans);
     
     if scans ~= 1054
        fprintf('Warning! Irregular number of scans. %s instead of 1054.\n',num2str(scans)) 
     end
     
     % get IC timecourse for good ICs and find outlier volumes
     good_outliers = zeros(length(IC_good),scans);
     
     for i = 1:length(IC_good)

         t=load(['/Volumes/FB-LIP/Projects/StateSwitch/dynamic/data/mri/task/preproc/B_data/D_preproc/' ID{sub} '/preproc2/' run_ID{run} '/FEAT.feat/filtered_func_data.ica/report/t' num2str(IC_good(i)) '.txt']);
         
         good_outliers(i,:) = (abs(t)>=thresh)'; % custom threshold value inserted

     end
     
     % get IDs of good ICs with outliers
     good_outliers_ID = IC_good(find(sum(good_outliers,2)));
     
     % get indices of volumes that contain outlying values across all good
     % components containing any number of outliers (can be empty arrays)
     good_outliers=good_outliers(find(sum(good_outliers,2)),:); % subset to good ICs containing any outliers
     
     if ~isempty(good_outliers)
        multiple_outlier_vol = find(sum(good_outliers,1) == size(good_outliers,1));
     else
        multiple_outlier_vol = [];
     end
     
     % create binary time-series index of bad volumes
     multiple_outlier_bin = zeros(1,scans);
     multiple_outlier_bin(multiple_outlier_vol) = 1;
     
     % add info to output structure  
     out.(['sub' ID{sub}]).(['run' run_ID{run}(5)]).multi_outlier_vol = multiple_outlier_vol;
     out.(['sub' ID{sub}]).(['run' run_ID{run}(5)]).multi_outlier_ind = multiple_outlier_bin;
     out.(['sub' ID{sub}]).(['run' run_ID{run}(5)]).outlier_IC = good_outliers_ID;
     out.(['sub' ID{sub}]).(['run' run_ID{run}(5)]).outlier_IC_N = length(good_outliers_ID);
     out.(['sub' ID{sub}]).(['run' run_ID{run}(5)]).IC_N = good_IC_N;
    
     clear HLN IC_noise IC_good good_IC_N good_outliers t good_outliers_ID multiple_outlier_vol multiple_outlier_bin
     
    end
end

% save stuff
save(['/Volumes/FB-LIP/Projects/StateSwitch/dynamic/data/mri/task/preproc/A_scripts/B3_preproc2/motion_outlier_processing/IC_outlier_thresh-' num2str(thresh) '.mat'],'out')