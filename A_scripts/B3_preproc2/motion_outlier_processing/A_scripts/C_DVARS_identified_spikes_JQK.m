% get multiple outlier indices for each subject and run according to
% timecourses of good ICs (not removed during denoising)
clear all
cd('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/preproc/A_scripts/B3_preproc2/motion_outlier_processing')

ID = {'1117' '1118' '1120' '1124' '1125' '1126' '1131' '1132' '1135' '1136' '1151' '1160' '1164' '1167' '1169' '1172' '1173' '1178' '1182' '1214' '1215' '1216' '1219' '1223' '1227' '1228' '1233' '1234' '1237' '1239' '1240' '1243' '1245' '1247' '1250' '1252' '1257' '1261' '1265' '1266' '1268' '1270' '1276' '1281' '2104' '2107' '2108' '2112' '2118' '2120' '2121' '2123' '2125' '2129' '2130' '2131' '2132' '2133' '2134' '2135' '2139' '2140' '2142' '2145' '2147' '2149' '2157' '2160' '2201' '2202' '2203' '2205' '2206' '2209' '2210' '2211' '2213' '2214' '2215' '2216' '2217' '2219' '2222' '2224' '2226' '2227' '2236' '2237' '2238' '2241' '2244' '2246' '2248' '2250' '2251' '2252' '2253' '2254' '2255' '2258' '2261'};
run_ID = {'run-1' 'run-2' 'run-3' 'run-4'};

pn.root = '/Volumes/LNDG/Projects/StateSwitch/';
pn.DVARS = [pn.root, 'dynamic/data/mri/task/preproc/B_data/D_preproc/'];

thresh = 5; % SD outlier threshold

for sub = 1:length(ID)
    for run = 1:length(run_ID)
     
     % get good components for run
     HLN=[pn.root, 'dynamic/data/mri/task/preproc/B_data/D_preproc/' ID{sub} '/preproc2/' run_ID{run} '/FEAT.feat/hand_labels_noise.txt'];
     
     % check if run exists
     if ~isfile(HLN)
        fprintf('No %s for subject %s ... skipping\n',run_ID{run},ID{sub})
        continue
     else
         fprintf('Processing subject %s %s\n',ID{sub},run_ID{run})
     end
     
     [~,IC_noise] = system(['tail -n -1 ' HLN]);
     IC_noise = str2num(IC_noise);
     IC_good = setdiff(1:30,IC_noise);
     
     % get total number of good components
     good_IC_N = length(IC_good);
     
     % get length of time-series
     scans=load([pn.root, 'dynamic/data/mri/task/preproc/B_data/D_preproc/' ID{sub} '/preproc2/' run_ID{run} '/FEAT.feat/filtered_func_data.ica/report/t1.txt']);
     scans=length(scans);
     
     if scans ~= 1054
        fprintf('Warning! Irregular number of scans. %s instead of 1054.\n',num2str(scans)) 
     end
     
     % get IC timecourse for good ICs and find outlier volumes
     good_outliers = zeros(length(IC_good),scans);
     
     for i = 1:length(IC_good)

         t=load([pn.root, 'dynamic/data/mri/task/preproc/B_data/D_preproc/' ID{sub} '/preproc2/' run_ID{run} '/FEAT.feat/filtered_func_data.ica/report/t' num2str(IC_good(i)) '.txt']);
         
         good_outliers(i,:) = (abs(t)>=thresh)'; % custom threshold value inserted

     end
     
     % create summary vector of all outlying volumes across good components
     IC_out_bin = zeros(1,scans);
     IC_out_bin(find(sum(good_outliers,1))) = 1;
     
     IC_out_N = length(find(IC_out_bin));
     
     % load DVARS output
     dvars_bin = (load([pn.root, 'dynamic/data/mri/task/preproc/B_data/D_preproc/' ID{sub} '/preproc/' run_ID{run} '/motionout/' ID{sub} '_motionout_scol.txt']))';
     
     dvars_out_N = length(find(dvars_bin));
     
     % calculate overlap and difference
     overlap_dvars_IC_N = length(intersect(find(dvars_bin),find(IC_out_bin)));
     dvars_not_IC_N = length(setdiff(find(dvars_bin),find(IC_out_bin)));
     IC_not_dvars = length(setdiff(find(IC_out_bin),find(dvars_bin)));
     
     % load DVARS toolbox output
     
     dataFile = [pn.DVARS, ID{sub}, '/preproc2/run-',num2str(run),'/',ID{sub}, '_run-',num2str(run),'_DVARS_tool.mat'];
     DVARSdata = load(dataFile);
     normalSignificance  = find(DVARSdata.DVARS_Stat.pvals<0.05./(DVARSdata.DVARS_Stat.dim(2)-1));
     signDVARS = find(DVARSdata.DVARS_Stat.DeltapDvar>5);
     signDVARS = intersect(normalSignificance,signDVARS);
     
     % calculate overlap and difference to previous DVARS
     
     overlap_dvars_toolbox_N = length(intersect(find(dvars_bin),signDVARS));
     dvars_not_toolbox_N = length(setdiff(find(dvars_bin),signDVARS));
     toolbox_not_dvars = length(setdiff(signDVARS,find(dvars_bin)));
     
     overlap_IC_toolbox_N = length(intersect(find(IC_out_bin),signDVARS));
     IC_not_toolbox_N = length(setdiff(find(IC_out_bin),signDVARS));
     toolbox_not_IC = length(setdiff(signDVARS,find(IC_out_bin)));
     
     % add info to output structure  
     out.(['sub' ID{sub}]).(['run' run_ID{run}(5)]).IC_out_N = IC_out_N;
     out.(['sub' ID{sub}]).(['run' run_ID{run}(5)]).dvars_out_N = dvars_out_N;
     out.(['sub' ID{sub}]).(['run' run_ID{run}(5)]).overlap_dvars_IC_N = overlap_dvars_IC_N;
     out.(['sub' ID{sub}]).(['run' run_ID{run}(5)]).dvars_not_IC_N = dvars_not_IC_N;
     out.(['sub' ID{sub}]).(['run' run_ID{run}(5)]).IC_not_dvars = IC_not_dvars;
     out.(['sub' ID{sub}]).(['run' run_ID{run}(5)]).good_IC_N = good_IC_N;
     
     out.(['sub' ID{sub}]).(['run' run_ID{run}(5)]).overlap_dvars_toolbox_N = overlap_dvars_toolbox_N;
     out.(['sub' ID{sub}]).(['run' run_ID{run}(5)]).dvars_not_toolbox_N = dvars_not_toolbox_N;
     out.(['sub' ID{sub}]).(['run' run_ID{run}(5)]).toolbox_not_dvars = toolbox_not_dvars;
     out.(['sub' ID{sub}]).(['run' run_ID{run}(5)]).overlap_IC_toolbox_N = overlap_IC_toolbox_N;
     out.(['sub' ID{sub}]).(['run' run_ID{run}(5)]).IC_not_toolbox_N = IC_not_toolbox_N;
     out.(['sub' ID{sub}]).(['run' run_ID{run}(5)]).toolbox_not_IC = toolbox_not_IC;
    
     clear HLN IC_noise IC_good IC_out_bin IC_out_N dvars_bin dvars_out_N overlap_dvars_IC_N dvars_not_IC_N IC_not_dvars
     
    end
    
    % summaries across runs
    % overlap
    out.(['sub' ID{sub}]).total_overlap_dvars_IC_N = 0;
    
    for run = 1:length(run_ID)
        if isfield(out.(['sub' ID{sub}]),['run' run_ID{run}(5)])
            out.(['sub' ID{sub}]).total_overlap_dvars_IC_N = out.(['sub' ID{sub}]).total_overlap_dvars_IC_N + out.(['sub' ID{sub}]).(['run' run_ID{run}(5)]).overlap_dvars_IC_N;
        end
    end
    
    % difference
    out.(['sub' ID{sub}]).total_dvars_not_IC_N = 0;
    
    for run = 1:length(run_ID)
        if isfield(out.(['sub' ID{sub}]),['run' run_ID{run}(5)])
            out.(['sub' ID{sub}]).total_dvars_not_IC_N = out.(['sub' ID{sub}]).total_dvars_not_IC_N + out.(['sub' ID{sub}]).(['run' run_ID{run}(5)]).dvars_not_IC_N;
        end
    end
    
    out.(['sub' ID{sub}]).total_IC_not_dvars = 0;
    
    for run = 1:length(run_ID)
        if isfield(out.(['sub' ID{sub}]),['run' run_ID{run}(5)])
            out.(['sub' ID{sub}]).total_IC_not_dvars = out.(['sub' ID{sub}]).total_IC_not_dvars + out.(['sub' ID{sub}]).(['run' run_ID{run}(5)]).IC_not_dvars;
        end
    end
    
    % summaries of dvars and IC methods separately
    out.(['sub' ID{sub}]).total_IC_out_N = 0;
    
    for run = 1:length(run_ID)
        if isfield(out.(['sub' ID{sub}]),['run' run_ID{run}(5)])
            out.(['sub' ID{sub}]).total_IC_out_N = out.(['sub' ID{sub}]).total_IC_out_N + out.(['sub' ID{sub}]).(['run' run_ID{run}(5)]).IC_out_N;
        end
    end
    
    out.(['sub' ID{sub}]).total_dvars_out_N = 0;
    
    for run = 1:length(run_ID)
        if isfield(out.(['sub' ID{sub}]),['run' run_ID{run}(5)])
            out.(['sub' ID{sub}]).total_dvars_out_N = out.(['sub' ID{sub}]).total_dvars_out_N + out.(['sub' ID{sub}]).(['run' run_ID{run}(5)]).dvars_out_N;
        end
    end
    
    % summary of good ICs
    out.(['sub' ID{sub}]).total_good_IC_N = 0;
    
    for run = 1:length(run_ID)
        if isfield(out.(['sub' ID{sub}]),['run' run_ID{run}(5)])
            out.(['sub' ID{sub}]).total_good_IC_N = out.(['sub' ID{sub}]).total_good_IC_N + out.(['sub' ID{sub}]).(['run' run_ID{run}(5)]).good_IC_N;
        end
    end
    
    % summary of joint information between methods
    
    out.(['sub' ID{sub}]).perc_overlap_dvars = out.(['sub' ID{sub}]).total_overlap_dvars_IC_N / out.(['sub' ID{sub}]).total_dvars_out_N * 100;
    out.(['sub' ID{sub}]).perc_overlap_IC = out.(['sub' ID{sub}]).total_overlap_dvars_IC_N / out.(['sub' ID{sub}]).total_IC_out_N * 100;
    
    fprintf('summary done.\n')
    
end

% save stuff
save([pn.root, 'dynamic/data/mri/task/preproc/A_scripts/B3_preproc2/motion_outlier_processing/comp2_IC_thr-' num2str(thresh) '_DVARS.mat'],'out')