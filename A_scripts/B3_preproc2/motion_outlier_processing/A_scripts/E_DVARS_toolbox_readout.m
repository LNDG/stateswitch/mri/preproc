% get DVARS output

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/preproc/B_data/D_preproc/1131/preproc2/run-1/1131_run-1_DVARS_tool.mat')
load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/preproc/B_data/D_preproc/1117/preproc2/run-1/1117_run-1_DVARS_tool.mat')
load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/preproc/B_data/D_preproc/1118/preproc2/run-2/1118_run-2_DVARS_tool.mat')


% DVARS_Stat.DeltapDvar
% 
% figure; plot(DVARS)
% 
% figure; plot(DVARS_Stat.DeltapDvar)
% figure; plot(DVARS_Stat.pvals)

% set a threshold at 10% of DeltapDvar values (Nichols paper suggests that adapting thresholds may be necessary)

% N = 43 YA + 53 OA;
IDs = {'1117';'1118';'1120';'1124';'1125';'1126';'1131';'1132';'1135';'1136';...
    '1151';'1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1214';'1215';...
    '1216';'1219';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';...
    '1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281';...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';'2130';...
    '2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';'2149';'2157';...
    '2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';'2211';'2213';'2214';...
    '2215';'2216';'2217';'2219';'2222';'2224';'2226';'2227';'2236';'2237';'2238';...
    '2241';'2244';'2246';'2248';'2250';'2251';'2252';'2258';'2261'};

pn.DVARS = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/preproc/B_data/D_preproc/';

thresholds = [3, 5, 10, 15];

% for indID = 1:numel(IDs)
%     disp(['Processing ', IDs{indID}])
%     for indRun = 1:4
%         try
%             dataFile = [pn.DVARS, IDs{indID}, '/preproc2/run-',num2str(indRun),'/',IDs{indID}, '_run-',num2str(indRun),'_DVARS_tool.mat'];
%             DVARSdata = load(dataFile);
%             for indThresh = 1:numel(thresholds)
%                 SignificantDVARS{indID,indRun,indThresh} = find(DVARSdata.DVARS_Stat.DeltapDvar >= thresholds(indThresh));
%                 SignificantDVARS_meanThresh(indID,indRun,indThresh) = mean(DVARSdata.DVARS_Stat.DeltapDvar(find(DVARS_Stat.DeltapDvar >= thresholds(indThresh))));
%             end
%         catch
%             disp('Error with analysis');
%             SignificantDVARS_meanThresh(indID,indRun,1:numel(thresholds)) = NaN;
%         end
%     end
% end
% 
% figure; bar(squeeze(nanmean(nanmean(SignificantDVARS_meanThresh,2),1)))
% 
% SignificantDVARS_num = cellfun(@numel, SignificantDVARS);
% 
% figure; bar(squeeze(nanmean(nanmean(SignificantDVARS_num,2),1)))
% 
% figure; hold on;
% histogram(SignificantDVARS_num(:,:,1), 20)
% histogram(SignificantDVARS_num(:,:,2), 20)
% histogram(SignificantDVARS_num(:,:,3), 20)
% histogram(SignificantDVARS_num(:,:,4), 20)
% % SignificantDVARS{1,1,3}
% % SignificantDVARS{1,2,3}


% remove outliers at threshold 5 or 10% of largest DVARS volumes

SignificantDVARS = []; SignificantDVARS_meanThresh = [];
for indID = 1:numel(IDs)
    disp(['Processing ', IDs{indID}])
    for indRun = 1:4
        try
            dataFile = [pn.DVARS, IDs{indID}, '/preproc2/run-',num2str(indRun),'/',IDs{indID}, '_run-',num2str(indRun),'_DVARS_tool.mat'];
            DVARSdata = load(dataFile);
            for indThresh = 1:numel(thresholds)
                normalSignificance  = find(DVARSdata.DVARS_Stat.pvals<0.05./(DVARSdata.DVARS_Stat.dim(2)-1));
                signDVARS = find(DVARSdata.DVARS_Stat.DeltapDvar>thresholds(indThresh));
                signDVARS = intersect(normalSignificance,signDVARS); %only if they are statistically sig as well!
                [sortVal, sortInd] = sort(DVARSdata.DVARS_Stat.DeltapDvar(:),'descend');
                if numel(signDVARS) > floor(length(sortVal)*0.1)
                    SignificantDVARS{indID,indRun,indThresh} = sortInd(1:ceil(length(Ms)*0.1));
                else
                    SignificantDVARS{indID,indRun,indThresh} = signDVARS;
                end
                SignificantDVARS_meanThresh(indID,indRun,indThresh) = mean(DVARSdata.DVARS_Stat.DeltapDvar(find(DVARS_Stat.DeltapDvar >= thresholds(indThresh))));
            end
        catch
            disp('Error with analysis');
            SignificantDVARS_meanThresh(indID,indRun,1:numel(thresholds)) = NaN;
        end
    end
end

SignificantDVARS_num = cellfun(@numel, SignificantDVARS);

figure; hold on;
%histogram(SignificantDVARS_num(:,:,1), 20)
histogram(SignificantDVARS_num(:,:,2), 20)
% histogram(SignificantDVARS_num(:,:,3), 20)
% histogram(SignificantDVARS_num(:,:,4), 20)
xlabel('Number of deleted volumes')
ylabel('Number of runs/subjects')
set(findall(gcf,'-property','FontSize'),'FontSize',18)

