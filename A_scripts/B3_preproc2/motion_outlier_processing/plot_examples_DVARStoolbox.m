% overlay DVARS on IC component
clear all

pn.root = '/Volumes/LNDG/Projects/StateSwitch/';
pn.DVARS = [pn.root, 'dynamic/data/mri/task/preproc/B_data/D_preproc/'];

% subject with most overlap at threshold 3 (2217,run4)
% get good components for run
thresh = 3;
HLN=[pn.root, 'dynamic/data/mri/task/preproc/B_data/D_preproc/2217/preproc2/run-4/FEAT.feat/hand_labels_noise.txt'];

[~,IC_noise] = system(['tail -n -1 ' HLN]);
IC_noise = str2num(IC_noise);
IC_good = setdiff(1:30,IC_noise);

good_outliers = zeros(length(IC_good),1054);

 for i = 1:length(IC_good)

   t=load([pn.root, 'dynamic/data/mri/task/preproc/B_data/D_preproc/2217/preproc2/run-4/FEAT.feat/filtered_func_data.ica/report/t' num2str(IC_good(i)) '.txt']);
         
   good_outliers(i,:) = (abs(t)>=thresh)'; % custom threshold value inserted

 end
 
IC_good_outliers = IC_good(find(sum(good_outliers,2)));

dvars_bin=load([pn.root, 'dynamic/data/mri/task/preproc/B_data/D_preproc/2217/preproc2/run-4/motionout_post/2217_run-4_feat_detrended_highpassed_denoised_motionout_post_scol.txt']);

h = figure('units','normalized','position',[.1 .1 .6 .8]);
for pl = 1:length(IC_good_outliers)
    % load FSL-based DVARS outliers
    IC_t=load([pn.root, 'dynamic/data/mri/task/preproc/B_data/D_preproc/2217/preproc2/run-4/FEAT.feat/filtered_func_data.ica/report/t' num2str(IC_good_outliers(pl)) '.txt']);
    % load DVARS toolbox output
    dataFile = [pn.DVARS, '2217', '/preproc2/run-','4','/','2217', '_run-','4','_DVARS_tool.mat'];
    DVARSdata = load(dataFile);
    normalSignificance  = find(DVARSdata.DVARS_Stat.pvals<0.05./(DVARSdata.DVARS_Stat.dim(2)-1));
    signDVARS = find(DVARSdata.DVARS_Stat.DeltapDvar>5);
    signDVARS = intersect(normalSignificance,signDVARS);
    subplot(length(IC_good_outliers),1,pl); hold on;
        xlim([1 1066])    
        xl=xlim;
        p = patch([0,0,xl(2),xl(2)],[thresh,-thresh,-thresh,thresh],[.9 .9 .9]);
        p.EdgeColor = 'none';%patches.colorVec(indP,:);
        p.FaceAlpha = 1;
        ylim([-6,6])
        ylims1 = [0 6]; ylims2 = [-6 0];
        line(repmat((find(dvars_bin))',2,1),repmat(ylims1',1,length(find(dvars_bin))),'Color',[.5 .5 .5], 'LineWidth', 2);
        % add practically significant DVARS
        line(repmat((signDVARS),2,1),repmat(ylims2',1,length(signDVARS)),'Color',[1 .5 .5], 'LineWidth', 2)
        % add data
        plot(IC_t, 'LineWidth', 2, 'Color', 'k')
        %line([xlim',xlim'],[3,-3;3,-3],'Color',[1,1,0])
        %xlim([1 1066])
        %xl=xlim;
        ylabel({'z-score';'amplitude'});
        title(['sub-2217 run-4 IC ' num2str(IC_good_outliers(pl))], 'FontSize', 10)
end
xlabel('Time (volumes)')
set(findall(gcf,'-property','FontSize'),'FontSize',18)

pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/preproc/A_scripts/B3_preproc2/motion_outlier_processing/C_figures/';
figureName = 'example_2217_run4';
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');

% saveas(gcf,'example_2217_run4.jpg')
clf('reset')

%% subject with least overlap at threshold 3 (1270,run3)
% get good components for run
thresh = 3;
HLN=[pn.root, 'dynamic/data/mri/task/preproc/B_data/D_preproc/1270/preproc2/run-3/FEAT.feat/hand_labels_noise.txt'];

[~,IC_noise] = system(['tail -n -1 ' HLN]);
IC_noise = str2num(IC_noise);
IC_good = setdiff(1:30,IC_noise);

good_outliers = zeros(length(IC_good),1054);

 for i = 1:length(IC_good)

   t=load([pn.root, 'dynamic/data/mri/task/preproc/B_data/D_preproc/1270/preproc2/run-3/FEAT.feat/filtered_func_data.ica/report/t' num2str(IC_good(i)) '.txt']);
         
   good_outliers(i,:) = (abs(t)>=thresh)'; % custom threshold value inserted

 end
 
IC_good_outliers = IC_good(find(sum(good_outliers,2)));

dvars_bin=load([pn.root, 'dynamic/data/mri/task/preproc/B_data/D_preproc/1270/preproc2/run-3/motionout_post/1270_run-3_feat_detrended_highpassed_denoised_motionout_post_scol.txt']);

for pl = 1:length(IC_good_outliers)

    % load FSL-based DVARS outliers
    IC_t=load([pn.root, 'dynamic/data/mri/task/preproc/B_data/D_preproc/1270/preproc2/run-3/FEAT.feat/filtered_func_data.ica/report/t' num2str(IC_good_outliers(pl)) '.txt']);
    % load DVARS toolbox output
    dataFile = [pn.DVARS, '1270', '/preproc2/run-','3','/','1270', '_run-','3','_DVARS_tool.mat'];
    DVARSdata = load(dataFile);
    normalSignificance  = find(DVARSdata.DVARS_Stat.pvals<0.05./(DVARSdata.DVARS_Stat.dim(2)-1));
    signDVARS = find(DVARSdata.DVARS_Stat.DeltapDvar>5);
    signDVARS = intersect(normalSignificance,signDVARS);
    subplot(length(IC_good_outliers),1,pl); hold on;
        p = patch([0,0,xl(2),xl(2)],[thresh,-thresh,-thresh,thresh],[.9 .9 .9]);
        p.EdgeColor = 'none';%patches.colorVec(indP,:);
        p.FaceAlpha = 1;
        ylim([-6,6])
        ylims1 = [0 6]; ylims2 = [-6 0];
        line(repmat((find(dvars_bin))',2,1),repmat(ylims1',1,length(find(dvars_bin))),'Color',[.5 .5 .5], 'LineWidth', 2);
        % add practically significant DVARS
        line(repmat((signDVARS),2,1),repmat(ylims2',1,length(signDVARS)),'Color',[1 .5 .5], 'LineWidth', 2)
        % add data
        plot(IC_t, 'LineWidth', 2, 'Color', 'k')
        %line([xlim',xlim'],[3,-3;3,-3],'Color',[1,1,0])
        xlim([1 1066])
        xl=xlim;
        ylabel({'z-score';'amplitude'});
        title(['sub-1270 run-3 IC ' num2str(IC_good_outliers(pl))], 'FontSize', 10)
end

figureName = 'example2_1270_run3';
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');% saveas(gcf,'example_1270_run3.jpg')
clf('reset')

%% subject with most significant DVARS values: 1182 run3

% get good components for run
thresh = 3;
HLN=[pn.root, 'dynamic/data/mri/task/preproc/B_data/D_preproc/1182/preproc2/run-3/FEAT.feat/hand_labels_noise.txt'];

[~,IC_noise] = system(['tail -n -1 ' HLN]);
IC_noise = str2num(IC_noise);
IC_good = setdiff(1:30,IC_noise);

good_outliers = zeros(length(IC_good),1054);

 for i = 1:length(IC_good)

   t=load([pn.root, 'dynamic/data/mri/task/preproc/B_data/D_preproc/1182/preproc2/run-3/FEAT.feat/filtered_func_data.ica/report/t' num2str(IC_good(i)) '.txt']);
         
   good_outliers(i,:) = (abs(t)>=thresh)'; % custom threshold value inserted

 end
 
IC_good_outliers = IC_good(find(sum(good_outliers,2)));

dvars_bin=load([pn.root, 'dynamic/data/mri/task/preproc/B_data/D_preproc/1182/preproc2/run-3/motionout_post/1182_run-3_feat_detrended_highpassed_denoised_motionout_post_scol.txt']);

h = figure('units','normalized','position',[.1 .1 .6 .8]);
for pl = 1:length(IC_good_outliers)
    % load FSL-based DVARS outliers
    IC_t=load([pn.root, 'dynamic/data/mri/task/preproc/B_data/D_preproc/1182/preproc2/run-3/FEAT.feat/filtered_func_data.ica/report/t' num2str(IC_good_outliers(pl)) '.txt']);
    % load DVARS toolbox output
    dataFile = [pn.DVARS, '1182', '/preproc2/run-','3','/','1182', '_run-','3','_DVARS_tool.mat'];
    DVARSdata = load(dataFile);
    normalSignificance  = find(DVARSdata.DVARS_Stat.pvals<0.05./(DVARSdata.DVARS_Stat.dim(2)-1));
    signDVARS = find(DVARSdata.DVARS_Stat.DeltapDvar>50);
    signDVARS = intersect(normalSignificance,signDVARS);
    signDVARS = signDVARS+1;
    [sortVal, sortInd] = sort(DVARSdata.DVARS_Stat.DeltapDvar(:),'descend');
    signDVARS_maximumCrit = sortInd(1:ceil(length(sortVal)*0.1));
    signDVARS_maximumCrit = signDVARS_maximumCrit+1;
    subplot(length(IC_good_outliers),1,pl); hold on;
        p = patch([0,0,xl(2),xl(2)],[thresh,-thresh,-thresh,thresh],[.9 .9 .9]);
        p.EdgeColor = 'none';%patches.colorVec(indP,:);
        p.FaceAlpha = 1;
        ylim([-6,6])
        ylims1 = [0 6]; ylims2 = [-6 0];
        line(repmat((find(dvars_bin))',2,1),repmat(ylims1',1,length(find(dvars_bin))),'Color',[.5 .5 .5], 'LineWidth', 2);
        % add practically significant DVARS
        line(repmat((signDVARS),2,1),repmat(ylims2',1,length(signDVARS)),'Color',[1 .5 .5], 'LineWidth', 2)
        %line(repmat((signDVARS_maximumCrit'),2,1),repmat(ylims2',1,length(signDVARS_maximumCrit')),'Color',[.5 1 .5], 'LineWidth', 2)
        % add data
        plot(IC_t, 'LineWidth', 2, 'Color', 'k')
        %line([xlim',xlim'],[3,-3;3,-3],'Color',[1,1,0])
        xlim([1 1066])
        xl=xlim;
        ylabel({'z-score';'amplitude'});
        title(['sub-1182 run-3 IC ' num2str(IC_good_outliers(pl))], 'FontSize', 10)
end
xlabel('Time (volumes)')
set(findall(gcf,'-property','FontSize'),'FontSize',18)

figureName = 'example3_1182_run3';
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');
%clf('reset')

%% run on bad components

clf('reset')

% get good components for run
thresh = 3;
HLN=[pn.root, 'dynamic/data/mri/task/preproc/B_data/D_preproc/1182/preproc2/run-3/FEAT.feat/hand_labels_noise.txt'];

[~,IC_noise] = system(['tail -n -1 ' HLN]);
IC_noise = str2num(IC_noise);
IC_good = setdiff(1:30,IC_noise);

good_outliers = zeros(length(IC_noise),1054);

 for i = 1:length(IC_noise)

   t=load([pn.root, 'dynamic/data/mri/task/preproc/B_data/D_preproc/1182/preproc2/run-3/FEAT.feat/filtered_func_data.ica/report/t' num2str(IC_noise(i)) '.txt']);
         
   good_outliers(i,:) = (abs(t)>=thresh)'; % custom threshold value inserted

 end
 
IC_good_outliers = IC_noise(find(sum(good_outliers,2)));

dvars_bin=load([pn.root, 'dynamic/data/mri/task/preproc/B_data/D_preproc/1182/preproc2/run-3/motionout_post/1182_run-3_feat_detrended_highpassed_denoised_motionout_post_scol.txt']);

h = figure('units','normalized','position',[.1 .1 .6 .8]);
for pl = 1:length(IC_good_outliers)
    % load FSL-based DVARS outliers
    IC_t=load([pn.root, 'dynamic/data/mri/task/preproc/B_data/D_preproc/1182/preproc2/run-3/FEAT.feat/filtered_func_data.ica/report/t' num2str(IC_good_outliers(pl)) '.txt']);
    % load DVARS toolbox output
    dataFile = [pn.DVARS, '1182', '/preproc2/run-','3','/','1182', '_run-','3','_DVARS_tool.mat'];
    DVARSdata = load(dataFile);
    normalSignificance  = find(DVARSdata.DVARS_Stat.pvals<0.05./(DVARSdata.DVARS_Stat.dim(2)-1));
    signDVARS = find(DVARSdata.DVARS_Stat.DeltapDvar>5);
    signDVARS = intersect(normalSignificance,signDVARS);
    signDVARS = signDVARS+1;
    [sortVal, sortInd] = sort(DVARSdata.DVARS_Stat.DeltapDvar(:),'descend');
    signDVARS_maximumCrit = sortInd(1:ceil(length(sortVal)*0.1));
    signDVARS_maximumCrit = signDVARS_maximumCrit+1;
    subplot(length(IC_good_outliers),1,pl); 
    hold on;
        p = patch([0,0,xl(2),xl(2)],[thresh,-thresh,-thresh,thresh],[.9 .9 .9]);
        p.EdgeColor = 'none';%patches.colorVec(indP,:);
        p.FaceAlpha = 1;
        ylim([-6,6])
        ylims1 = [0 6]; ylims2 = [-6 0];
        line(repmat((find(dvars_bin))',2,1),repmat(ylims1',1,length(find(dvars_bin))),'Color',[.5 .5 .5], 'LineWidth', 2);
        % add practically significant DVARS
        line(repmat((signDVARS),2,1),repmat(ylims2',1,length(signDVARS)),'Color',[1 .5 .5], 'LineWidth', 2)
        %line(repmat((signDVARS_maximumCrit'),2,1),repmat(ylims2',1,length(signDVARS_maximumCrit')),'Color',[.5 1 .5], 'LineWidth', 2)
        % add data
        plot(IC_t, 'LineWidth', 2, 'Color', 'k')
        %line([xlim',xlim'],[3,-3;3,-3],'Color',[1,1,0])
        xlim([1 1066])
        xl=xlim;
        %ylabel({'z-score';'amplitude'});
        if pl == 1
            title(['sub-1182 run-3 IC ' num2str(IC_good_outliers(pl))], 'FontSize', 10)
        end
        set(gca, 'XTick', []);
end
xlabel('Time (volumes)')
set(findall(gcf,'-property','FontSize'),'FontSize',18)

figureName = 'example3_1182_run3_badcomps';
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');
