#!/bin/bash

# 170109 | JQK changed path to standards directory 

source ../preproc_config.sh

if [ ! -d ${ScriptsPath}/extras/registration ]; then mkdir -p ${ScriptsPath}/extras/registration ; fi

Files=""
InputStage="feat_detrended_bandpassed_FIX_MNI${VoxelSize}mm"

# Loop over participants, sessions (if they exist) & runs/conditions/tasks/etc
for SUB in ${SubjectID} ; do
	if [ -z "${SessionID}" ]; then Session="NoSessions" ; SessionFolder="" ; SessionName=""
	else Session="${SessionID}"
	fi	
	for SES in ${Session}; do
		if [ "${Session}" != "NoSessions" ]; then
			if [ ! -d ${DataPath}/${SUB}/${SES} ]; then continue
			else SessionFolder="${SES}/"; SessionName="${SES}_"
			fi			
		fi
		for RUN in ${RunID}; do
			
			# Path to the original functional image folder.
			OriginalPath="${DataPath}/${SUB}/${SessionFolder}mri/${RUN}"
			# Name of functional image to be used.
			FuncImage="${SUB}_${SessionName}${RUN}"	
			# Path to the pipeline specific folder.
			FuncPath="${DataPath}/${SUB}/${SessionFolder}${PreprocPipe}/${RUN}"	
			
			if [ ! -f ${OriginalPath}/${FuncImage}.nii.gz ]; then
				continue
			elif [ ! -f ${FuncPath}/${FuncImage}_${InputStage}.nii.gz ]; then
				continue
			fi
	
			Files="${Files} ${FuncPath}/${FuncImage}_${InputStage}.nii.gz"
						
		done
	done
done

# Setup FSL
source /etc/fsl/5.0/fsl.sh

# Create functional image viewer with MNI overlay
cd ${ScriptsPath}/extras/registration
slicesdir -p ${StandardPath}/MNI152_T1_${VoxelSize}mm_brain.nii.gz ${Files}
