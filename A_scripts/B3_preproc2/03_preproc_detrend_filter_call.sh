#!/bin/bash

# 180111 | JQK increased memory allocation to 8GB

## Compiled Matlab matlab_master_do

source preproc2_config.sh

# Preprocessing suffix. Denotes the preprocessing stage of the data.
InputStage="feat" 		# Before doing steps on matlab
OutputStage="feat_detrended_highpassed" 		# After running Detrend & Filtering

# Test
#SubjectID="EYEMEMtest"

# PBS Log Info
CurrentPreproc="Detrend_Highpass"
CurrentLog="${LogPath}/${CurrentPreproc}"
if [ ! -d ${CurrentLog} ]; then mkdir ${CurrentLog}; chmod 770 ${CurrentLog}; fi

# Error log
Error_Log="${CurrentLog}_error_summary.txt"; echo "" >> ${Error_Log}; chmod 770 ${CurrentLog}

# Loop over participants, sessions (if they exist) & runs/conditions/tasks/etc
for SUB in ${SubjectID} ; do
	if [ -z "${SessionID}" ]; then Session="NoSessions"; SessionFolder=""; SessionName=""
	else Session="${SessionID}"
	fi
	for SES in ${Session}; do
		if [ "${Session}" != "NoSessions" ]; then
			if [ ! -d ${DataPath}/${SUB}/${SES} ]; then continue
			else SessionFolder="${SES}/"; SessionName="${SES}_"
			fi			
		fi	
		for RUN in ${RunID}; do
			
			# Name of the functional image to be used.
			FuncImage="${SUB}_${SessionName}${RUN}"												# Run specific functional image
			# Path to the functional image folder.
			FuncPath="${DataPath}/${SUB}/${SessionFolder}${PreprocPipe}/${RUN}"	# Path for run specific functional image
			
			if [ ! -f ${FuncPath}/FEAT.feat/filtered_func_data.nii.gz ]; then
				continue
			else
				if [ -f ${FuncPath}/${FuncImage}_${OutputStage}.nii.gz ] \
					&& [ ! -f ${FuncPath}/${FuncImage}_${InputStage}_detrended.nii.gz ] ; then
					continue
				fi
			fi

			# Gridwise
			echo "#PBS -N ${CurrentPreproc}_${FuncImage}" 			>> job # Job name 
			echo "#PBS -l walltime=2:00:00" 						>> job # Time until job is killed 
			echo "#PBS -l mem=8gb" 									>> job # Books 8gb RAM for the job 
			echo "#PBS -m n" 										>> job # Email notification on abort/end, use 'n' for no notification 
			echo "#PBS -o ${CurrentLog}" 							>> job # Write (output) log to group log folder 
			echo "#PBS -e ${CurrentLog}" 							>> job # Write (error) log to group log folder 

			echo "cd ${ScriptsPath}"	 							>> job
			
			# Inputs must be given in the correct order: 
				# FuncPath, FuncImage, PolyOrder, TR, HighpassFilterLowCutoff, LowpassFilterHighCutoff, FilterOrder
			echo -n "./run_preproc_detrend_filter_excecute.sh /opt/matlab/R2014b/ " 															>> job 
			echo "${FuncPath} ${FuncImage}_${InputStage} ${PolyOrder} ${TR} ${HighpassFilterLowCutoff} ${LowpassFilterHighCutoff} ${FilterOrder}" 	>> job  
			
			# Cleanup
			echo "if [ -f ${FuncPath}/${FuncImage}_${OutputStage}.nii.gz ];" 					>> job
			echo "then rm -rf ${FuncPath}/${FuncImage}_${InputStage}_detrended.nii.gz; fi" 		>> job
			
			# Error Log
			echo "if [ ! -f ${FuncPath}/${FuncImage}_${OutputStage}.nii.gz ];" 					>> job
			echo "then echo 'Error in ${FuncImage}' >> ${Error_Log}; fi"						>> job 
			
			qsub job
			rm job
			
		done
	done
done