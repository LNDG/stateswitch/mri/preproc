function extractFSLmotoutvols
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

workingdirectory = '/home/mpib/LNDG/StateSwitch/WIP/preproc/B_data/D_preproc';
subFolders={'1117' '1118' '1120' '1124' '1125' '1126' '1131' '1132' '1135' '1136' '1151' '1160' '1164' '1167' '1169' '1172' '1173' '1178' '1182' '1214' '1215' '1216' '1219' '1223' '1227' '1228' '1233' '1234' '1237' '1239' '1240' '1243' '1245' '1247' '1250' '1252' '1257' '1261' '1265' '1266' '1268' '1270' '1276' '1281' '2104' '2107' '2108' '2112' '2118' '2120' '2121' '2123' '2125' '2129' '2130' '2131' '2132' '2133' '2134' '2135' '2139' '2140' '2142' '2145' '2147' '2149' '2157' '2160' '2201' '2202' '2203' '2205' '2206' '2209' '2210' '2211' '2213' '2214' '2215' '2216' '2217' '2219' '2222' '2224' '2226' '2227' '2236' '2237' '2238' '2241' '2244' '2246' '2248' '2250' '2251' '2252' '2253' '2254' '2255' '2258' '2261'};

nruns=4;

for s = 1:length(subFolders)

    currentSubjDir = [workingdirectory '/' subFolders{s}];
    
    motrowbinall=[];
    
    for i = 1:nruns
        
        currentSubjRunDir=[currentSubjDir '/preproc2/run-' int2str(i)];
        
        if (exist([currentSubjRunDir '/motionout_post/' subFolders{s} '_run-' int2str(i) '_feat_detrended_highpassed_denoised_motionout_post.txt']))
            
            %Read in motion file
            motfile=dlmread([currentSubjRunDir '/motionout_post/' subFolders{s} '_run-' int2str(i) '_feat_detrended_highpassed_denoised_motionout_post.txt']);
            
            %Identify motion outlier volumes
            rowsum=sum(motfile,2);
            
            %Ensure max value is one within column
            motrowbin=rowsum;
            motrowbin(rowsum~=0)=1;
            
            motrowbinall=cat(1,motrowbinall,motrowbin);
            
            %Write out column
            dlmwrite([currentSubjRunDir '/motionout_post/' subFolders{s} '_run-' int2str(i) '_feat_detrended_highpassed_denoised_motionout_post_scol.txt'], motrowbin, 'delimiter', '\t');
            
        end
        
            dlmwrite([currentSubjDir '/preproc2/' subFolders{s} '_feat_detrended_highpassed_denoised_motionout_post_scol_all.txt'], motrowbinall, 'delimiter', '\t');
        
    end
    
end

end