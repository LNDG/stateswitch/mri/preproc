function preproc_detrend_filter_excecute(FuncPath, FuncImage, PolyOrder, TR, HighpassFilterLowCutoff, LowpassFilterHighCutoff, FilterOrder)
%% For compilation:

% /opt/matlab/R2014b/bin/mcc -m preproc_detrend_highpass_excecute -a /home/mpib/LNDG/EyeMem/scripts/toolboxes/NIFTI_toolbox -a /home/mpib/LNDG/EyeMem/scripts/toolboxes/spm_detrend -a /home/mpib/LNDG/EyeMem/scripts/preproc_matlab_steps
% /opt/matlab/R2014b/bin/mcc -m preproc_detrend_filter_excecute -a /home/mpib/ramirez/GridScriptTest/scripts/toolboxes/NIFTI_toolbox -a /home/mpib/ramirez/GridScriptTest/scripts/toolboxes/spm_detrend -a /home/mpib/ramirez/GridScriptTest/scripts/preproc/matlab_steps

% source preproc_config.sh; /opt/matlab/R2014b/bin/mcc -m ${PreprocPipe}_detrend_highpass_excecute -a ${SharedFilesPath}/toolboxes/NIFTI_toolbox -a ${SharedFilesPath}/toolboxes/spm_detrend -a ${ScriptsPath}/${PreprocPipe}_matlab_steps

disp (['Will attempt to process ' FuncImage ])

cd ([ FuncPath ]);
if ~exist([ FuncImage, '_detrended.nii.gz' ], 'file')
    preproc_detrend( [ FuncPath, '/FEAT.feat/filtered_func_data' ], [FuncPath '/' FuncImage] , PolyOrder, [FuncPath '/FEAT.feat/mask.nii.gz'] ); 
end

cd ([ FuncPath ]);
if ~exist([ FuncImage, '_detrended_bandpassed.nii.gz' ], 'file') || ~exist([ FuncImage, '_detrended_lowpassed.nii.gz' ], 'file')... 
        || ~exist([ FuncImage, '_detrended_highpassed.nii.gz' ], 'file') && exist([ FuncImage, '_detrended.nii.gz' ], 'file')
    preproc_filter ( [ FuncImage, '_detrended' ], TR, num2str(HighpassFilterLowCutoff), num2str(LowpassFilterHighCutoff), FilterOrder, [], [FuncPath '/FEAT.feat/mask.nii.gz']);
end

end
