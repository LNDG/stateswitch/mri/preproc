% check for components with spikes (+/- 6 z-score) (YA)
clear all

cd('/Users/skowron/Volumes/tardis/LNDG/StateSwitch/WIP/preproc/A_scripts/B3_preproc2/05_FIX')

ID_YA = {'1117' '1118' '1120' '1124' '1125' '1126' '1131' '1132' '1135' '1136' '1151' '1160' '1164' '1167' '1169' '1172' '1173' '1178' '1182' '1214' '1215' '1216' '1219' '1223' '1227' '1228' '1233' '1234' '1237' '1239' '1240' '1243' '1245' '1247' '1250' '1252' '1257' '1261' '1265' '1266' '1268' '1270' '1276' '1281'};
ID_OA = {'2104' '2107' '2108' '2112' '2118' '2120' '2121' '2123' '2125' '2129' '2130' '2131' '2132' '2133' '2134' '2135' '2139' '2140' '2145' '2147' '2149' '2157' '2160' '2201' '2202' '2203' '2205' '2206' '2209' '2210' '2211' '2213' '2214' '2215' '2216' '2217' '2219' '2222' '2224' '2226' '2227' '2236' '2237' '2238' '2241' '2244' '2246' '2248' '2250' '2251' '2252' '2258' '2261'};

ID_OA_part = {'2130' '2139' '2134' '2248' '2214' '2206' '2129' '2145' '2108' '2201' '2210' '2203' '2123' '2147' '2213' '2258' '2107' '2104' '2121' '2250' '2202' '2215' '2236'};

run_ID = {'1' '2' '3' '4'};

spike_report = {};

for sub = 1:length(ID_OA_part) % cycle over young adults
   for run = 1:length(run_ID)
        
        report_path=['/Users/skowron/Volumes/tardis/LNDG/StateSwitch/WIP/preproc/B_data/D_preproc/' ID_OA_part{sub} '/preproc2/run-' run_ID{run} '/FEAT.feat/filtered_func_data.ica/report'];
       
        if ~exist(report_path)
            fprintf('no report path found for %s run-%s\n', ID_OA_part{sub}, run_ID{run})
            continue
        end
       
    for IC = 1:30 % cycle over ICs
       
        t=dlmread([report_path '/t' num2str(IC) '.txt']);
        
        spikes=0;
        spikes=spikes + sum(t>=6);
        spikes=spikes + sum(t<=-6);
        
        if spikes ~= 0
            spike_report = [spike_report ; [ID_OA_part{sub} ' run-' run_ID{run} ' ' num2str(IC)]];
        end
        
    end
    
   end
end

save('spike_report_OA_part.mat','spike_report')