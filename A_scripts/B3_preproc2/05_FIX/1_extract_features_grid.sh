#!/bin/bash

## FIX: Feature Extraction

source ../preproc2_config.sh

# Preprocessing suffix. This denotes the preprocessing stage of the data, that is to say, the preprocessing steps which have already been undertaken before generating ICA.ica folder.
InputStage="feat_detrended_highpassed"

# Test
#SubjectID="EYEMEMtest"

# PBS Log Info
CurrentPreproc="Extract_Features"
CurrentLog="${LogPath}/05_FIX/${CurrentPreproc}"
if [ ! -d ${CurrentLog} ]; then mkdir -p ${CurrentLog}; chmod 770 ${CurrentLog}; fi

# Error Log
Error_Log="${LogPath}/05_FIX/${CurrentPreproc}_error_summary.txt"; echo "" >> ${Error_Log}; chmod 770 ${Error_Log}

# Loop over participants, sessions (if they exist) & runs
for SUB in ${SubjectID} ; do
	if [ -z "${SessionID}" ]; then
		Session="NoSessions"
		SessionFolder=""
		SessionName=""
	else
		Session="${SessionID}"
	fi
	for SES in ${Session}; do
		if [ "${Session}" != "NoSessions" ]; then
			if [ ! -d ${DataPath}/${SUB}/${SES} ]; then
				continue
			else
				SessionFolder="${SES}/"
				SessionName="${SES}_"
			fi			
		fi
		for RUN in ${RunID}; do
			
			# Name of functional image to be used.
			FuncImage="${SUB}_${SessionName}${RUN}"													# Run specific functional image
			# Path to the functional image folder.
			FuncPath="${DataPath}/${SUB}/${SessionFolder}${PreprocPipe}/${RUN}"		# Path for run specific functional image
			
			if [ ! -f ${FuncPath}/${FuncImage}_${InputStage}.nii.gz ]; then
				continue
			elif [ -d ${FuncPath}/FEAT.feat/fix ]; then
				cd ${FuncPath}/FEAT.feat/fix
				Log=`grep "End of Matlab Script" logMatlab.txt | tail -1` # Get line containing our desired text output
				if [ ! "$Log" == "End of Matlab Script" ]; then
					echo "${SUB} ${RUN}: fix was incomplete, deleting and re-running"
					rm -rf ${FuncPath}/FEAT.feat/fix
				else
					continue
				fi
			fi

			cd ${FuncPath}/FEAT.feat
			if [ ! -f filtered_func_data.nii.gz ]; then
				echo "${SUB} ${RUN}: filtered_func_data.nii.gz missing" >> ${Error_Log}
				continue
			elif [ ! -d filtered_func_data.ica ]; then
				echo "${SUB} ${RUN}: filtered_func_data.ica missing" >> ${Error_Log}
				continue	
			elif [ ! -f mc/prefiltered_func_data_mcf.par ]; then
				echo "${SUB} ${RUN}: filtered_func_data.ica missing" >> ${Error_Log}
				continue
			elif [ ! -f mask.nii.gz ]; then
				echo "${SUB} ${RUN}: mask.nii.gz missing" >> ${Error_Log}
				continue
			elif [ ! -f mean_func.nii.gz ]; then
				echo "${SUB} ${RUN}: mean_func.nii.gz missing" >> ${Error_Log}
				continue
			elif [ ! -f reg/example_func.nii.gz ]; then
				echo "${SUB} ${RUN}: reg/example_func.nii.gz missing" >> ${Error_Log}
				continue
			elif [ ! -f reg/highres.nii.gz ]; then
				echo "${SUB} ${RUN}: reg/highres.nii.gz missing" >> ${Error_Log}
				continue
			elif [ ! -f reg/highres2example_func.mat ]; then
				echo "${SUB} ${RUN}: reg/highres2example_func.mat missing" >> ${Error_Log}
				continue
			elif [ ! -f design.fsf ]; then
				echo "${SUB} ${RUN}: design.fsf missing" >> ${Error_Log}
				continue																								
			fi

			echo "#PBS -N ${CurrentPreproc}_${SUB}_${RUN}" 						>> job # job name 
			echo "#PBS -l walltime=24:00:00" 									>> job # time until job is killed 
			echo "#PBS -l mem=2gb" 												>> job # books 4gb RAM for the job 
			#echo "#PBS -m ae" 													>> job # email notification on abort/end   -n no notification 
			echo "#PBS -o ${CurrentLog}" 										>> job # write (error) log to group log folder 
			echo "#PBS -e ${CurrentLog}" 										>> job 

			FSLDIR="/home/mpib/LNDG/FSL/fsl-5.0.11"
			echo "FSLDIR=${FSLDIR}" 								>> job
			echo ". ${FSLDIR}/etc/fslconf/fsl.sh"                   >> job
			echo "PATH=${FSLDIR}/bin:${PATH}"                       >> job
			echo "export FSLDIR PATH"                               >> job
			
			#echo "source /etc/fsl/5.0/fsl.sh"									>> job 	 # set fsl environment
			#echo "which fix"													>> job
			echo "module load fsl_fix" 											>> job
			
			# Extract features for FIX; FEAT directories with all required files and folders have to be provided
			echo "cd ${FuncPath}"												>> job
			echo "fix -f ${FuncPath}/FEAT.feat" 								>> job 

			# Change group permissions for all documents created using this script
			echo "cd ${FuncPath}/FEAT.feat/" 									>> job
			#echo "chgrp -R lip-lndg ."  										>> job
			echo "chmod -R 770 ."  												>> job

			qsub job  
			rm job
			
		done
	done
done