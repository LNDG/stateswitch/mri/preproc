#!/bin/bash

source ../preproc2_config.sh

for SUB in ${SubjectID} ; do
	if [ -z "${SessionID}" ]; then
		Session="NoSessions"
		SessionFolder=""
		SessionName=""
	else
		Session="${SessionID}"
	fi
	for SES in ${Session}; do
		if [ "${Session}" != "NoSessions" ]; then
			if [ ! -d ${DataPath}/${SUB}/${SES} ]; then
				continue
			else
				SessionFolder="${SES}/"
				SessionName="${SES}_"
			fi			
		fi
		for RUN in ${RunID}; do
			
			# Name of functional image to be used.
			FuncImage="${SUB}_${SessionName}${RUN}"												# Run specific functional image
			# Path to the functional image folder.
			FuncPath="${DataPath}/${SUB}/${SessionFolder}${PreprocPipe}/${RUN}"	# Path for run specific functional image
			
			HLN_File="${FuncPath}/FEAT.feat/hand_labels_noise.txt"
			
			if [ ! -f ${HLN_File} ];then
				echo "HLN file missing for ${SUB} ${RUN}"
			fi
			
			if [ -f ${HLN_File} ] && [ $(cat -e ${HLN_File} | tail -c 2) != "$" ]; then
				echo "adding new line to ${FuncImage}"
				echo "" >> ${HLN_File}
			fi
			
			
		done
	done
done