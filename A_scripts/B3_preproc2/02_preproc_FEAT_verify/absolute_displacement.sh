#!/bin/bash

source ../preproc2_config.sh

if [ ! -d ${ScriptsPath}/extras ]; then mkdir -p ${ScriptsPath}/extras ; fi
Absolute="${ScriptsPath}/extras/Absolute_Displacement.txt"

# Loop over participants, sessions (if they exist) & runs/conditions/tasks/etc
for SUB in ${SubjectID} ; do
	if [ -z "${SessionID}" ]; then Session="NoSessions" ; SessionFolder="" ; SessionName=""
	else Session="${SessionID}"
	fi	
	for SES in ${Session}; do
		if [ "${Session}" != "NoSessions" ]; then
			if [ ! -d ${DataPath}/${SUB}/${SES} ]; then continue
			else SessionFolder="${SES}/"; SessionName="${SES}_"
			fi			
		fi
		for RUN in ${RunID}; do
			
			# Name of functional image to be used.
			FuncImage="${SUB}_${SessionName}${RUN}"	
			# Name of brain extracted anatomical and functional image to be used.
			AnatImage="${SUB}_${SessionName}t1_brain"
			# Path to the original functional image folder.
			OriginalPath="${DataPath}/${SUB}/${SessionFolder}mri/${RUN}"
			# Path to the pipeline specific folder.
			FuncPath="${DataPath}/${SUB}/${SessionFolder}${PreprocPipe}/${RUN}"	
			# Path to the anatomical imaged
			AnatPath="${DataPath}/${SUB}/${SessionFolder}mri/t1"
	
			if [ ! -f ${OriginalPath}/${FuncImage}.nii.gz ]; then
				continue
			elif [ -d ${FuncPath}/FEAT.feat ]; then
				if [ ! -f ${FuncPath}/FEAT.feat/filtered_func_data.nii.gz ]; then
					echo "${SUB} ${SessionName} ${RUN} did not run properly" >> ${Absolute}
					continue
				fi
			fi
			
			# Get Absolute Displacement Value
			cd ${FuncPath}/FEAT.feat 
			AbsoluteVAL=`grep "absolute" report_prestats.html` # Get line containing our desired index/number
			AbsoluteVAL=`echo ${AbsoluteVAL:72}` # Cut out text prior to our index/number
			AbsoluteVAL=`echo ${AbsoluteVAL%%m*}` # Cut out text following our index/number
			
			# Print Value
			W=`grep "warning" report_prestats.html` # Get line containing our desired text output
			if [[ $W == *"warning"* ]]; then  
				echo "${AbsoluteVAL}mm:		${SUB} ${SessionName} ${RUN} WARNING PRESENT, VERIFY PRE-STATS" >> ${Absolute}
			else
				echo "${AbsoluteVAL}mm:		${SUB} ${SessionName} ${RUN}" >> ${Absolute}
			fi
			
		done
	done
done