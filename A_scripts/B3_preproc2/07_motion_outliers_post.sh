source preproc2_config.sh

# PBS Log Info
CurrentPreproc="mo_out_post"
CurrentLog="${LogPath}/${CurrentPreproc}"
if [ ! -d ${CurrentLog} ]; then mkdir -p ${CurrentLog}; chmod 770 ${CurrentLog}; fi
	
FSLDIR=/home/mpib/LNDG/FSL/fsl-5.0.11

# Error log
Error_Log="${CurrentLog}/${CurrentPreproc}_error_summary.txt"; echo "" >> ${Error_Log}; chmod 770 ${CurrentLog}


#SubjectID="1228"
#RunID="run-1"

# Loop over participants, sessions (if they exist) & runs/conditions/tasks/etc
for SUB in ${SubjectID} ; do
	if [ -z "${SessionID}" ]; then Session="NoSessions"; SessionFolder=""; SessionName=""
	else Session="${SessionID}"
	fi
	for SES in ${Session}; do
		if [ "${Session}" != "NoSessions" ]; then
			if [ ! -d ${DataPath}/${SUB}/${SES} ]; then continue
			else SessionFolder="${SES}/"; SessionName="${SES}_"
			fi			
		fi
		for RUN in ${RunID}; do
			
			# Name of anatomical and functional images to be used.
			FuncImage="${SUB}_${SessionName}${RUN}_feat_detrended_highpassed_denoised"												# Run specific functional image
			# Path to the anatomical and functional image folders.
			FuncPath="${DataPath}/${SUB}/${SessionFolder}${PreprocPipe}/${RUN}"	# Path for run specific functional image
							
			if [ ! -f ${FuncPath}/${FuncImage}.nii.gz ]; then
				continue
			elif [ -f ${FuncPath}/motionout_post/${FuncImage}_motionout_post.txt ]; then
				continue
			fi
					
			# Create output path for motion outlier detection
			
			if [ -d ${FuncPath}/motionout_post ]; then 
				if [ ! -f ${FuncPath}/motionout_post/${FuncImage}_motionout_post.txt ]; then
					rm -rf ${FuncPath}/motionout_post
				fi
			fi	
			if [ ! -d ${FuncPath}/motionout_post ]; then mkdir -p ${FuncPath}/motionout_post; fi
			
			# Gridwise
			echo "#PBS -N ${CurrentPreproc}_${FuncImage}" 						>> job # Job name 
			echo "#PBS -l walltime=12:00:00" 									>> job # Time until job is killed 
			echo "#PBS -l mem=5gb" 											>> job # Books 4gb RAM for the job 
			echo "#PBS -m n" 													>> job # Email notification on abort/end, use 'n' for no notification 
			echo "#PBS -o ${CurrentLog}" 										>> job # Write (output) log to group log folder 
			echo "#PBS -e ${CurrentLog}" 										>> job # Write (error) log to group log folder 

			#echo ". /etc/fsl/5.0/fsl.sh"										>> job # Set fsl environment 	
			
			echo "FSLDIR=/home/mpib/LNDG/FSL/fsl-5.0.11"  >> job
			echo ". ${FSLDIR}/etc/fslconf/fsl.sh"                   >> job
			echo "PATH=${FSLDIR}/bin:${PATH}"                       >> job
			echo "export FSLDIR PATH"                               >> job
			
			echo "cd ${FuncPath}/motionout_post"           												>> job

			# Run motion outlier detection
			echo "fsl_motion_outliers -i ${FuncPath}/${FuncImage}.nii.gz -o ${FuncPath}/motionout_post/${FuncImage}_motionout_post.txt -s ${FuncPath}/motionout_post/${FuncImage}_${MoutMetric}.txt -p ${FuncPath}/motionout_post/${FuncImage}_${MoutMetric}_plot.png --${MoutMetric} --nomoco -v >> ${FuncPath}/motionout_post/report.txt" >> job
			
			#echo "fsl_motion_outliers -i ${OriginalPath}/${FuncImage} -o ${FuncPath}/motionout/${SUB}_motionout.txt -s ${FuncPath}/motionout/${SUB}_${MoutMetric}.txt -p ${FuncPath}/motionout/${SUB}_${MoutMetric}_plot.png --${MoutMetric} --dummy=${DeleteVolumes} -v >> ${FuncPath}/motionout/report.txt" >> job
			
			# Error log
			echo "if [ ! -f ${FuncPath}/motionout_post/${FuncImage}_motionout_post.txt ];"  		>> job
			echo "then echo 'Error in ${FuncImage}' >> ${Error_Log}; fi"				>> job
			
			echo "chmod -R 770 ${FuncPath}/motionout_post" >> job
			
			qsub job
			rm job
			
		done
	done
done