#!/bin/bash
#Template file which sets up PBS requirements for all jobs

# All required resource statements start with "#PBS -l"
# These will be interpreted by the job submission system (PBS pro)

#PBS -v ncpus=8,MRTRIX=/opt/software/mrtrix3/rc3
#PBS -l nodes=1:ppn=1,mem=6G
# *select* is the number of parallel processes
# *ncpus* is the number of cores required by each process
# *mem* is the amount of memory required by each process

#PBS -l walltime=23:00:00
# *walltime* is the total time required in hours:minutes:seconds
# to run the job. 
# Warning: all processes still running at the end of this period
# of time will be killed and any temporary data will be erased.

#PBS -m abe 
#NOTE: if you want to receive notifications about job status, enter your email after the "abe" above
# *m* situations under which to email a) aborted b) begin e) end
# *M* email address to send emails to

export OMP_NUM_THREADS=8

####
#Find all scripting files
#currdir=$(pwd)
pipedir=/home/mpib/LNDG/StateSwitch/WIP/preproc/A_scripts/I_replaceFEATregs
PATH=$PATH$( find $pipedir/ -not -path '*/\.*'  -type d -printf ":%p" )
#export PATH:$PATH


####
#Load all required software
module load mrtrix3/rc3
module load freesurfer/6.0.0
module load ants/2.1.0

#Manually add other software
FSLDIR=/home/mpib/LNDG/FSL/fsl-5.0.11
. ${FSLDIR}/etc/fslconf/fsl.sh
PATH=${FSLDIR}/bin:${PATH}
othdir=/home/mpib/LNDG/toolboxes/c3dtool/bin
export FSLDIR PATH=${othdir}:$PATH

#Extract ID of subject and their corresponding folder holding raw image files

subj=1240
WORKDIR=/home/mpib/LNDG/StateSwitch/WIP/preproc/B_data/D_preproc

rsyncoldFEATfolds 1240 /home/mpib/LNDG/StateSwitch/WIP/preproc/B_data/D_preproc  #"%%%%" will match to the string  that corresponds to your personalised batch script, or preferred pipeline
 
