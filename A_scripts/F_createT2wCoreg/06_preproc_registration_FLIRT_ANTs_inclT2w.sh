#!/bin/bash

## FLIRT: Registration

## This will register functional images to anatomical images and then to MNI space. See 'Run Flirt commands' section for more information.

# 170109 | JQK changed path to standards directory
# 180718 | JQK changed BET to ANTs version
# 		 | JQK changed BOLD to BET T1w to 6 DOF (from 12)

source preproc_config_tardis.sh

# Preprocessing suffix. Denotes the preprocessing stage of the data.
InputStage="feat_detrended_bandpassed_FIX" 			# Before running FLIRT
OutputStage="${InputStage}_MNI${VoxelSize}mm" 		# After running FLIRT

# Test
#SubjectID="EYEMEMtest"

# PBS Log Info
CurrentPreproc="FLIRT"
CurrentLog="${LogPath}/${CurrentPreproc}"
if [ ! -d ${CurrentLog} ]; then mkdir ${CurrentLog}; chmod 770 ${CurrentLog}; fi

# Error log
Error_Log="${CurrentLog}_error_summary.txt"; echo "" >> ${Error_Log}; chmod 770 ${CurrentLog}

# Loop over participants, sessions (if they exist) & runs/conditions/tasks/etc
for SUB in ${SubjectID} ; do
	if [ -z "${SessionID}" ]; then
		Session="NoSessions"
		SessionFolder=""
		SessionName=""
	else
		Session="${SessionID}"
	fi
	for SES in ${Session}; do
		if [ "${Session}" != "NoSessions" ]; then
			if [ ! -d ${DataPath}/${SUB}/${SES} ]; then
				continue
			else
				SessionFolder="${SES}/"
				SessionName="${SES}_"
			fi			
		fi
		for RUN in ${RunID}; do
			
			# Name of anatomical and functional images to be used.
			FuncImage="${SUB}_${SessionName}${RUN}"											# Run specific functional image
			T1wImage="${SUB}_${SessionName}t1_ANTs_brain"									# Brain extracted anatomical image (T1w)
			T2wImage="${SUB}_${SessionName}T2w_ANTs_brain"									# Brain extracted anatomical image (T2w)
			# Path to the anatomical and functional image folders.
			T1wPath="${DataPath}/${SUB}/${SessionFolder}mri/t1"								# Path for anatomical image (T1w)
			T2wPath="${DataPath}/${SUB}/${SessionFolder}mri/t2"								# Path for anatomical image (T2w)
			FuncPath="${DataPath}/${SUB}/${SessionFolder}${PreprocPipe}/${RUN}"				# Path for run specific functional image
			
			if [ ! -f ${FuncPath}/${FuncImage}_${InputStage}.nii.gz ]; then
				continue
			#elif [ -f ${FuncPath}/${FuncImage}_${OutputStage}.nii.gz ]; then
			#	continue 
			fi
		
			# Gridwise
			echo "#PBS -N ${CurrentPreproc}_${FuncImage}" 			>> job # Job name 
			echo "#PBS -l walltime=1:00:00" 						>> job # Time until job is killed 
			echo "#PBS -l mem=4gb" 									>> job # Books 4gb RAM for the job 
			echo "#PBS -m n" 										>> job # Email notification on abort/end, use 'n' for no notification 
			echo "#PBS -o ${CurrentLog}" 							>> job # Write (output) log to group log folder 
			echo "#PBS -e ${CurrentLog}" 							>> job # Write (error) log to group log folder 
			
			# Initialize FSL
			echo ". /etc/fsl/5.0/fsl.sh"							>> job # Set fsl environment 			
		
			# Variables for FLIRT
			
			Preproc="${FuncPath}/${FuncImage}_${InputStage}.nii.gz"									# Preprocessed data image
			Registered="${FuncPath}/${FuncImage}_${OutputStage}.nii.gz"								# Registered image
			T2w_BET="${T2wPath}/${T2wImage}.nii.gz"
			T1w_BET="${T1wPath}/${T1wImage}.nii.gz"													# Brain extracted anatomical image
			MNI="${StandardPath}/MNI152_T1_${VoxelSize}mm_brain.nii.gz"								# Standard MNI image
			Preproc_to_T2w="${FuncPath}/FEAT.feat/reg/${FuncImage}_Preproc_to_T1w.mat"				# Func to T2w registration matrix
			T2w_to_T1w="${FuncPath}/FEAT.feat/reg/${FuncImage}_T2w_to_T1w.mat"		    			# T2w to T1w registration matrix
			T1w_to_MNI="${FuncPath}/FEAT.feat/reg/${FuncImage}_T1w_to_MNI.mat"				    	# T1w to MNI registration matrix
			Preproc_to_MNI="${FuncPath}/FEAT.feat/reg/${FuncImage}_preproc_to_MNI.mat"		    	# Final registration matrix

			## Run FLIRT commands
		
			# Create lowres registration matrix. Register preprocessed data to T2w BET image and create preproc_to_BET matrix (A1_to_A2)
			echo "flirt -in ${Preproc} -ref ${T2w_BET} -omat ${Preproc_to_T2w} -dof 6"  						>> job
			# Register T2w BET to T1w BET image and create T2w_to_T1w matrix (A2_to_B)
			echo "flirt -in ${T2w_BET} -ref ${T1w_BET} -omat ${T2w_to_T1w} -dof 6"  							>> job			
			# Register T1w BET image to MNI space and create BET_to_MNI matrix (B_to_C)
			echo "flirt -in ${T1w_BET} -ref ${MNI} -omat ${T1w_to_MNI} -dof 12"  								>> job
			# Create final registration matrix. Combine previously created matrices into preproc_to_MNI (A_to_C). This will be used for creating the registered images.
			# omat = name of concatenated matrices (A_to_C), 
			# concat = two matrices to be concatenated (B_to_C A_to_B)
			echo "convert_xfm -omat ${Preproc_to_MNI} -concat ${T1w_to_MNI} ${T2w_to_T1w} ${Preproc_to_T2w}"   	>> job
			# Register preprocessed data to MNI space using MNI image and combined matrices
			echo "flirt -in ${Preproc} -ref ${MNI} -out ${Registered} -applyxfm -init ${Preproc_to_MNI}"  		>> job
			
			echo "chmod 770 ${Registered}"																		>> job
			
			# Error Log
			echo "if [ ! -f ${FuncPath}/${FuncImage}_${OutputStage}.nii.gz ];" 									>> job
			echo "then echo 'Error in ${FuncImage}' >> ${Error_Log}; fi"										>> job 
			
			qsub job
			rm job
			
		done
	done
done
