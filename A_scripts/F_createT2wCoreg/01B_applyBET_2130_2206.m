# Create masked T2w BET images for ANTs-failed subjects
# 190103 | JQK created

rootPath='/Users/kosciessa/Desktop/mntTardisLNDG/LNDG/StateSwitch/WIP/preproc/B_data/D_preproc/2130/mri/t2/'
fslmaths ${rootPath}/2130_T2w.nii.gz -mul ${rootPath}/tempANTs/2130_T2w_ANTsBET_BrainExtractionPriorWarped.nii.gz ${rootPath}/2130_T2w_ANTs_brain.nii.gz

rootPath='/Users/kosciessa/Desktop/mntTardisLNDG/LNDG/StateSwitch/WIP/preproc/B_data/D_preproc/2206/mri/t2/'
fslmaths ${rootPath}/2206_T2w.nii.gz -mul ${rootPath}/tempANTs/2206_T2w_ANTsBET_BrainExtractionPriorWarped.nii.gz ${rootPath}/2206_T2w_ANTs_brain.nii.gz

#rootPath='/Users/kosciessa/Desktop/mntTardisLNDG/LNDG/StateSwitch/WIP/preproc/B_data/D_preproc/2226/mri/t2/'
#fslmaths ${rootPath}/2226_T2w.nii.gz -mul ${rootPath}/tempANTs/2226_T2w_ANTsBET_BrainExtractionPriorWarped.nii.gz ${rootPath}/2226_T2w_ANTs_brain.nii.gz

# Use MATLAB script for 2226, the above multiplication in FSL doesn't work and results in an empty image 