#!/bin/bash

## Configuration file
# This file will set environments and variables which will be used throughout the preprocessing procedures.

# 170109 | JQK changed path to standards directory, DataPath

## Study parameters

# Project name. This should be the name of the folder in which the study data is saved.
ProjectName="STSWD"									# No default, must be set by user

# Name of this specific pipeline.
	# EX: "new_preproc"
PreprocPipe="preproc" 							# Default is preproc

# Set subject ID list. Use an explicit list. No commas between subjects.
SubjectID="1117 1118 1120 1124 1126 1131 1132 1135 1136 1138 1144 1151 1158 1160 1163 1164 1167 1169 1172 1173 1178 1182 1213 1215 1216 1219 1221 1223 1227 1228 1233 1234 1237 1239 1240 1243 1245 1247 1250 1252 1257 1261 1265 1266 1268 1270 1276 1281 2104 2107 2108 2112 2118 2120 2121 2123 2125 2129 2130 2131 2132 2133 2134 2135 2139 2140 2142 2145 2147 2149 2157 2160 2201 2202 2203 2205 2206 2209 2210 2211 2213 2214 2215 2216 2217 2219 2222 2224 2226 2227 2236 2237 2238 2241 2244 2246 2248 2250 2251 2252 2253 2254 2255 2258 2261" 									# No default, must be set by user

#4224 removed as no structural or functional data are available

# Set session ID list. Leave as an empty string if no sessions in data path. No commas if containing any session information.
	# EX: SessionID="ses1 ses2"
SessionID="" 									# Default is empty string

# Name of experimental conditions, runs or task data to be analyzed. No commas between runs/conditions.
	# EX: RunID="task1_run1 task1_run2 task2 restingstate"
RunID="run-1 run-2 run-3 run-4" 				# No default, must be set by user

# Voxel size for registration
VoxelSize="3" 									# No default, must be set by user

# Additional parameters
StandardsAndMasks="standards" 					# Used for FEAT & FLIRT Registration

## Set directories

BaseDirectory="/home/mpib/LNDG/StateSwitch/WIP/preproc"

## Project directories
ProjectDirectory="${BaseDirectory}"  							# Base project directory
ScriptsPath="${ProjectDirectory}/A_scripts/B_${PreprocPipe}" 	# Pipe specific scripts
LogPath="${ProjectDirectory}/Y_logs/B_${PreprocPipe}"			# Common log paths for pipe
#SharedFilesPath="${ProjectDirectory}/scripts" 					# Common Toolboxes, Standards, etc
ToolboxPath="${ProjectDirectory}/D_tools" 						# Common Toolboxes
StandardPath="${ProjectDirectory}/B_data/C_standards" 			# Standards
DataPath="${ProjectDirectory}/B_data/D_preproc"  			# Data directory

# Initiate project logs
if [ ! -d ${LogPath} ]; then mkdir -p ${LogPath}; chmod 770 ${LogPath}; fi