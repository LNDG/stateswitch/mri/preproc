ID="1117 1118 1120 1124 1125 1126 1131 1132 1135 1136 1151 1160 1164 1167 1169 1172 1173 1178 1182 1214 1215 1216 1219 1223 1227 1228 1233 1234 1237 1239 1240 1243 1245 1247 1250 1252 1257 1261 1265 1266 1268 1270 1276 1281 2104 2107 2108 2112 2118 2120 2121 2123 2125 2129 2130 2131 2132 2133 2134 2135 2139 2140 2142 2145 2147 2149 2157 2160 2201 2202 2203 2205 2206 2209 2210 2211 2213 2214 2215 2216 2217 2219 2222 2224 2226 2227 2236 2237 2238 2241 2244 2246 2248 2250 2251 2252 2253 2254 2255 2258 2261"

task_runs="1 2 3 4"

RawPath="/Volumes/FB-LIP/Projects/StateSwitch/dynamic/data/mri/task/preproc/B_data/D_preproc"

# fsl_motion_outliers settings
DeleteVolumes="12"
mout_metric="dvars"

for SUB in ${ID}; do
	for RUN in ${task_runs}; do
	
		InPath="${RawPath}/${SUB}/mri/run-${RUN}"
		OutPath="${RawPath}/${SUB}/preproc/run-${RUN}/motionout"
	
		if [[ -d ${InPath} ]]; then
			
			if [[ ! -d ${OutPath} ]]; then
				mkdir -p ${OutPath}
			fi
			
			fsl_motion_outliers -i ${InPath}/${SUB}_run-${RUN} -o ${OutPath}/${SUB}_motionout.txt --dummy=${DeleteVolumes} -s ${OutPath}/${SUB}_${mout_metric}.txt -p ${OutPath}/${SUB}_${mout_metric}_plot.png --${mout_metric} -v >> ${OutPath}/report.txt
			
		else
			echo "No run ${RUN} for subject ${SUB}."
		fi
	
	done
done